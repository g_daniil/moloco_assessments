package moloco

func EqualsWhenOneCharRemoved(x, y string) bool {  
	lenDiff := len(x) - len(y)
	// if the absolute difference in length is not equal 1 then removing 1 character won't work
	// could use a "math.Abs" here to avoid double comparison. chose to not import the package
	if lenDiff != 1 && lenDiff != -1 {
		return false
	}
	var a, b string
	if lenDiff == 1 {
		a = x
		b = y
	} else {
		a = y
		b = x
	}
	for i := 0; i < len(a); i++ {
		if i == len(b) || a[i] != b[i] {
			// character doesn't match
			// remove the character from a lengthier string
			modifiedA := a[0:i] + a[i+1:len(a)]
			// no need to do a further check if strings don't match at this point
			return modifiedA == b
		}
		// there is a potential optimization for this 
		// if the function will walk the string from both ends simultaniously
	}
	return false
}