package main

import (
	"io"
	"encoding/json"
    "fmt"
    "log"
	"os"
	"strings"
)

type Order struct {
    User_Id string
    Product_Id string
    Quantity int
}

func main() {

	if len(os.Args) < 2 {
        log.Fatal("Please specify filename as a first argument")
	}
	
	fileName := os.Args[1]
	
    file, err := os.Open(fileName)
    if err != nil {
        log.Fatal(err)
    }
    defer file.Close()

	var o Order

	uniqueBuyers := make(map[string]map[string]struct{})
	topUniqueBuyers := 0
	topUniqueBuyersProducts := make(map[string]struct{})

	totalQuantities := make(map[string]int)
	topQuantity := 0
	topQuantityProducts := make(map[string]struct{})

	dec := json.NewDecoder(file)
    for {
		err := dec.Decode(&o)

        if err == io.EOF {
            break
        }
        if err != nil {
            log.Fatal(err)
        }

		// process unique buyers
		if uniqueBuyers[o.Product_Id] == nil {
			uniqueBuyers[o.Product_Id] = make(map[string]struct{})
		}
		uniqueBuyers[o.Product_Id][o.User_Id] = struct{}{}

		if len(uniqueBuyers[o.Product_Id]) > topUniqueBuyers {
			topUniqueBuyers = len(uniqueBuyers[o.Product_Id])
			topUniqueBuyersProducts = make(map[string]struct{})
			topUniqueBuyersProducts[o.Product_Id] = struct{}{}
		} else if len(uniqueBuyers[o.Product_Id]) == topUniqueBuyers {
			topUniqueBuyersProducts[o.Product_Id] = struct{}{}
		}

		//process quantities
		totalQuantities[o.Product_Id] += o.Quantity

		if totalQuantities[o.Product_Id] > topQuantity {
			topQuantity = totalQuantities[o.Product_Id]
			topQuantityProducts = make(map[string]struct{})
			topQuantityProducts[o.Product_Id] = struct{}{}
		} else if totalQuantities[o.Product_Id] == topQuantity {
			topQuantityProducts[o.Product_Id] = struct{}{}
		}
	}
	
	fmt.Println("Most popular product(s) based on the number of purchasers: "  + convertMapToKeysString(topUniqueBuyersProducts))
	fmt.Println("Most popular product(s) based on the quantity of goods sold: " + convertMapToKeysString(topQuantityProducts))
}

func convertMapToKeysString(mapToConvert map [string]struct{}) string {
	keys := make([]string, 0, len(mapToConvert))
	for key := range mapToConvert {
		keys = append(keys, key)
	}
	return strings.Join(keys, ",")
}