-- Consider only the rows with country_id = "BDV" (there are 844 such rows). For each site_id, 
-- we can compute the number of unique user_id's found in these 844 rows. Which site_id has 
-- the largest number of unique users? And what's the number?
SELECT 
	site_id, COUNT(DISTINCT(user_id)) as cnt
FROM 
	moloco_q3
WHERE 
	country_id = 'BDV'
GROUP BY 
	site_id
ORDER BY 
	cnt DESC;

-- Between 2019-02-03 00:00:00 and 2019-02-04 23:59:59, there are four users who visited 
-- a certain site more than 10 times. Find these four users & which sites they (each) 
-- visited more than 10 times. (Simply provides four triples in the form (user_id, site_id, number of visits) in the box below.)
SELECT 
	user_id, site_id, COUNT(*) as cnt
FROM 
	moloco_q3
WHERE 
	ts >= '2019-02-03 00:00:00' 
	AND 
	ts <= '2019-02-04 23:59:59'
GROUP BY
	user_id, site_id
HAVING 
	COUNT(*) >= 10;


-- For each site, compute the unique number of users whose last visit (found in the original data set) 
-- was to that site. For instance, user "LC3561"'s last visit is to "N0OTG" based on timestamp data. 
-- Based on this measure, what are top three sites? (hint: site "3POLC" is ranked at 5th with 28 users 
-- whose last visit in the data set was to 3POLC; simply provide three pairs in the form (site_id, number of users).)

SELECT 
	t1.site_id, COUNT(t1.user_id) as cnt
FROM
	moloco_q3 as t1
LEFT JOIN
	moloco_q3 as t2
ON 
	t1.user_id = t2.user_id
	AND
	t1.ts < t2.ts
WHERE 
	t2.ts IS NULL
GROUP BY
	t1.site_id
ORDER BY 
	cnt DESC;
	
	
-- For each user, determine the first site he/she visited and the last site he/she visited 
-- based on the timestamp data. Compute the number of users whose first/last visits are to the same website. 
-- What is the number?

SELECT COUNT(1) FROM (
	SELECT 
		user_id, MIN(ts) as first_visit, MAX(ts) as last_visit
	FROM 
		moloco_q3
	GROUP BY
		user_id
) as user_visits
INNER JOIN (
	SELECT 
		user_id, site_id, MIN(ts) as first_visit, MAX(ts) as last_visit
	FROM 
		moloco_q3
	GROUP BY
		user_id, site_id
) as user_site_visits
ON 
	user_visits.user_id = user_site_visits.user_id
	AND
	user_visits.first_visit = user_site_visits.first_visit
	AND
	user_visits.last_visit = user_site_visits.last_visit;